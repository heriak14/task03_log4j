package com.heriak;

import com.heriak.console.Console;
import com.heriak.console.WarnInfo;
import com.heriak.file.*;
import com.heriak.mail.SMS;
import com.heriak.mail.SMTP;

public class Main {
    public static void main(String[] args) {
        Console.printLogs();                //prints logs in console and file
        SimpleFile.printLogs();             //prints logs in file without overwriting
        DailyRollingFile.printLogs();       //prints logs in file with overwriting it every day
        SizeTriggeredRollFile.printLogs();  //prints logs in file with overwriting it if size > 1 MB
        DailyFile.printLogs();              //prints logs in new file everyday
        ErrorFile.printLogs();              //prints error+ level logs in file
        WarnInfo.printLogs();               //prints only warn logs in file and only info in console
        SMTP.printLogs();                   //sends error logs via mail
        SMS.send("This is fatal message!!!"); //sends fatal logs via sms

    }
}
