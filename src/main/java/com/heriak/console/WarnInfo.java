package com.heriak.console;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WarnInfo {

    private static final Logger logger = LogManager.getLogger(WarnInfo.class);

    public static void printLogs() {
        logger.trace("This is trace log");
        logger.debug("This is debug log");
        logger.info("This is info log");
        logger.warn("This is warn log");
        logger.error("This is error log");
        logger.fatal("This is fatal log");
    }
}
