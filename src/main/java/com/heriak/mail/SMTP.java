package com.heriak.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SMTP {

    private static final Logger logger = LogManager.getLogger(SMTP.class);

    public static void printLogs() {
        logger.trace("This is trace log");
        logger.debug("This is debug log");
        logger.info("This is info log");
        logger.warn("This is warn log");
        logger.error("This is error log");
        logger.fatal("This is fatal log");
    }
}
